package com.java.footwear;

public interface FeatureInterface {

	void lace();
	void sole();
	void color();

}

