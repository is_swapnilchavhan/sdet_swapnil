package com.java.footwear;

public class ShoesUsage {

	enum Usage
	{ FORMAL, CASUAL, SPORTS }

	enum UsageType
	{ BUCKLED, SLIPON,LACEUP }
	
	enum Buckled
	{
		MONKSTRAP
	}
	
	enum SLIPON
	{
		LOAFER, MOCASSIN
	}
	enum LACEUP
	{
		OXFORD, DERBY, BLUCHER, BUDAPESTER
	}
	
}
