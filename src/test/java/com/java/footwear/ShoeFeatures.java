package com.java.footwear;

public class ShoeFeatures extends $RunnerClass implements FeatureInterface{

	@Override
	public void lace() {
		System.out.println("\nAvailable in with and without lace");
		
	}

	@Override
	public void sole() {
		
		System.out.println("Available material: Leather, Denim");
	}

	@Override
	public void color() {
	
		System.out.println("Available color: Black , Brown, White");
	}

	
}
