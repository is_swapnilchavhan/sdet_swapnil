package com.java.footwear;

import java.util.Scanner;

import com.java.footwear.ShoesUsage.Buckled;
import com.java.footwear.ShoesUsage.LACEUP;
import com.java.footwear.ShoesUsage.SLIPON;

public class $RunnerClass {

	public static Scanner input;		
	public static String selectedUsage;
	public static String selectedShoeStyle;

	public static void main(String[] args) {

		System.out.println("Welcome to the shop , Below are shoe type available:");
		ShoesUsage.Usage [] u = ShoesUsage.Usage.values();
		for(ShoesUsage.Usage v:u) 
			System.out.println(v);
		System.out.println("Please enter shoe type:");
		
		input = new Scanner(System.in);
		selectedUsage = input.nextLine().toLowerCase();

		if(selectedUsage.contentEquals("formal")||selectedUsage.contentEquals("casual") ||selectedUsage.contentEquals("sports") ) {
			System.out.println("\nWhat style of material do you want to use? :");
			ShoesUsage.UsageType [] ut = ShoesUsage.UsageType.values();
			for(ShoesUsage.UsageType vt:ut) 
				System.out.println(vt);
			System.out.println("Please enter the type of material:");
			
			selectedShoeStyle = input.nextLine().toLowerCase();
			if(selectedShoeStyle.contentEquals("buckled")||selectedShoeStyle.contentEquals("slipon")||selectedShoeStyle.contentEquals("laceup")) 
			{

				System.out.println("\nSelected "+selectedShoeStyle+" style "+selectedUsage+" shoes are available in:");

				if(selectedShoeStyle.equals("buckled")) 
				{
					
					for(Buckled v:ShoesUsage.Buckled.values()) 
						System.out.println(v);
					
					System.out.println("Available in below features:\n");
					ShoeFeatures sf = new ShoeFeatures();
					sf.lace();
					sf.sole();
					sf.color();

				}
				else if(selectedShoeStyle.equals("slipon")) 
				{
					for(SLIPON v:ShoesUsage.SLIPON.values()) 
						System.out.println(v);
					ShoeFeatures sf = new ShoeFeatures();
					sf.lace();
					sf.sole();
					sf.color();

					
				}

				else if(selectedShoeStyle.equals("laceup")) 
				{
					for(LACEUP v:ShoesUsage.LACEUP.values()) 
						System.out.println(v);
					ShoeFeatures sf = new ShoeFeatures();
					sf.lace();
					sf.sole();
					sf.color();

				}

			}
			else {
				System.out.println("This style is not available!!\n");
			}

		}
		else {
			System.out.println("This usage is not available!!\n");
		}

		input.close();
	}

}
